package com.eisuchi.eisuchi.ui.base

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import com.commonProject.ui.dialog.ProgressDialog
import com.eisuchi.dialog.LogoutDialog
import com.eisuchi.extention.dismissAlertDialog
import com.eisuchi.utils.SessionManager
import com.eisuchi.eisuchi.R
import com.eisuchi.eisuchi.data.modal.BaseModal
import com.eisuchi.eisuchi.ui.login.LoginActivity
import com.eisuchi.eisuchi.uitils.Constant
import com.eisuchi.extention.goToActivityAndClearTask
import com.eisuchi.extention.showAlert
import com.google.gson.Gson

/**
 * Created by Hardik
 */

open class BaseActivity : AppCompatActivity() {
    var title: TextView? = null
    var toolbar: Toolbar? = null
    lateinit var session: SessionManager

    var shouldPerformDispatchTouch = true
    var progressDialog: ProgressDialog? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = SessionManager(this)
        disableAutoFill()
    }

    /*fun isThisMe(userId: String?): Boolean {
        return userId == session.user?._id
    }*/

    fun errorResponse(response:String){
        val gson = Gson()
        val baseModal=gson.fromJson(response, BaseModal::class.java)
        if (baseModal?.error?.code==6){
            logOutDialog(this)
        }else{
            showAlert(baseModal.error?.msg.toString())
        }
    }

    // LogOut Dialog Show
    fun logOutDialog(context: Context){

        val dialog = LogoutDialog.newInstance(
            context,
            object : LogoutDialog.onItemClick {
                override fun onItemCLicked() {
                    session.clearSession()
                    goToActivityAndClearTask<LoginActivity>()
                }
            })
        val bundle = Bundle()
        bundle.putString(Constant.TITLE, getString(R.string.app_name))
        bundle.putString(Constant.TEXT, "Invalid session key")
        bundle.putBoolean(Constant.INFO, true)
        dialog.arguments = bundle
        dialog.show(supportFragmentManager, "YesNO")
    }

    override fun onPause() {
        super.onPause()
        hideSoftKeyboard()
    }

    override fun onDestroy() {
        dismissAlertDialog()
        hideSoftKeyboard()
        super.onDestroy()
    }

    fun showProgressbar() {
        showProgressbar(null)
    }

    fun showProgressbar(message: String? = getString(R.string.please_wait)) {

        hideProgressbar()
        progressDialog = ProgressDialog(this, message)
        progressDialog?.show()
    }

    fun hideProgressbar() {
        if (progressDialog != null && progressDialog?.isShowing!!) progressDialog!!.dismiss()
    }

    /* fun setUpToolbar(strTitle: String) {
         setUpToolbarWithBackArrow(strTitle, false)
     }*/

/*    @JvmOverloads
    fun setUpToolbarWithBackArrow(strTitle: String? = null, isBackArrow: Boolean = true) {
        toolbar = findViewById(R.id.toolbar)
        title = toolbar?.findViewById(R.id.tvTitle)

        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false)
            actionBar.setDisplayHomeAsUpEnabled(isBackArrow)
            actionBar.setHomeAsUpIndicator(R.drawable.v_ic_arrow_back)
            if (strTitle != null) title?.text = strTitle
        }
    }*/

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                hideSoftKeyboard()
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun showSoftKeyboard(view: EditText) {
        view.requestFocus(view.text.length)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun hideSoftKeyboard(): Boolean {
        try {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            return imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
            return false
        }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        var ret = false
        try {
            val view = currentFocus
            ret = super.dispatchTouchEvent(event)
            if (shouldPerformDispatchTouch) {
                if (view is EditText) {
                    val w = currentFocus
                    val scrCords = IntArray(2)
                    if (w != null) {
                        w.getLocationOnScreen(scrCords)
                        val x = event.rawX + w.left - scrCords[0]
                        val y = event.rawY + w.top - scrCords[1]

                        if (event.action == MotionEvent.ACTION_UP && (x < w.left || x >= w.right || y < w.top || y > w.bottom)) {
                            val imm =
                                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
                        }
                    }
                }
            }
            return ret
        } catch (e: Exception) {
            e.printStackTrace()
            return ret
        }

    }

    fun disableAutoFill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            window.decorView.importantForAutofill =
                View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
        }
    }

    companion object {

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        }
    }

    open fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {}
}
