package com.eisuchi.eisuchi.ui.orderDetail


import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.eisuchi.utils.SessionManager

import com.eisuchi.eisuchi.R
import com.eisuchi.eisuchi.data.modal.OrderItems
import com.eisuchi.eisuchi.databinding.RowOrderIteamsBinding
import com.eisuchi.extention.hide
import com.eisuchi.extention.invisible
import com.eisuchi.extention.visible


class OrderIteamAdapter(
    private val mContext: Context,
    var list: MutableList<OrderItems> = mutableListOf(),
    var session: SessionManager,
    var status: String,
    private val listener: OrderIteamAdapter.OnItemSelected,
) : RecyclerView.Adapter<OrderIteamAdapter.ItemHolder>() {

    lateinit var binding: RowOrderIteamsBinding

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        binding = RowOrderIteamsBinding.inflate(
            LayoutInflater
                .from(parent.getContext()), parent, false
        )
        return ItemHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, session)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: OrderItems, action: String)
    }

    class ItemHolder(containerView: RowOrderIteamsBinding) : RecyclerView.ViewHolder(containerView.root) {
        val binding = containerView

        fun bindData(
            context: Context,
            data: OrderItems,
            listener: OnItemSelected, session: SessionManager
        ) {

            Glide.with(context).load(data.imageUrl).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(binding.imageView2)
            if (data.status=="0"){
                binding.btnDelivered.visible()
                binding.txtStatus.text = "Pending"
                binding.txtStatus.setTextColor(Color.parseColor("#E03744"))
            }else{
                binding.btnDelivered.hide()
                binding.txtStatus.text = "Deliverd"
                binding.txtStatus.setTextColor(Color.parseColor("#008000"))
            }

            binding.txtIteamName.text = data.itemName
            binding.txtIteamQty.text = "Total Qty: ${data.qty}, "
            val pendingQty = data.qty?.toInt()?.minus(data.deliveredQty?.toInt()!!)
            binding.txtPendingQty.text = "Pending Qty: $pendingQty"
            binding.btnDelivered.setOnClickListener { listener.onItemSelect(adapterPosition, data, "MainView") }

        }
    }


}