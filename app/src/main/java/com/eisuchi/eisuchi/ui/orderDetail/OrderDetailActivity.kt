package com.eisuchi.eisuchi.ui.orderDetail


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.eisuchi.eisuchi.R
import com.eisuchi.eisuchi.data.modal.DeliveryModal
import com.eisuchi.eisuchi.data.modal.OrderDetailModal
import com.eisuchi.eisuchi.data.modal.OrderItems
import com.eisuchi.eisuchi.data.network.ApiHelper
import com.eisuchi.eisuchi.data.network.Networking
import com.eisuchi.eisuchi.databinding.ActivityOrderDetailBinding
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.eisuchi.ui.base.ViewModelProviderFactory
import com.eisuchi.eisuchi.uitils.Constant
import com.eisuchi.eisuchi.uitils.Status
import com.eisuchi.eisuchi.uitils.TimeStamp
import com.eisuchi.dialog.SetDeliveryDialog
import com.eisuchi.extention.*
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.lang.Exception
import java.util.concurrent.TimeUnit


class OrderDetailActivity : BaseActivity(), OrderIteamAdapter.OnItemSelected {
    lateinit var binding: ActivityOrderDetailBinding
    var adapter: OrderIteamAdapter? = null
    private lateinit var orderDetailViewModal: OrderDetailViewModal
    private val list: MutableList<OrderItems> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.toolbar.txtTitle.text =getString(R.string.order_details)
        binding.toolbar.txtLogout.invisible()
        binding.toolbar.imgBack.setImageResource(R.drawable.ic_baseline_arrow)
        setupViewModel()
        clickEvent()
        setupRecyclerView()
        getOrderDetail()
    }

    private fun setupViewModel() {
        val factory =   ViewModelProviderFactory(ApiHelper(Networking(this).getServices()))
        orderDetailViewModal = ViewModelProvider(this, factory).get(OrderDetailViewModal::class.java)
    }

    // Recyclerview Init
    fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.include.recyclerView.layoutManager = layoutManager
        adapter = OrderIteamAdapter(this, list, session, "", this)
        binding.include.recyclerView.adapter = adapter

    }

    // View Click Handle
    fun clickEvent(){
        binding.toolbar.imgBack.setOnClickListener { finish() }
    }

    // OrderList API calling
    fun  getOrderDetail(){
        list.clear()
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("order_id", intent.getStringExtra(Constant.DATA))
            jsonBody.put("sessionKey", session.user.data?.sessionKey)

            result = Networking.setParentJsonData(
                Constant.ORDER,
                jsonBody
            )
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        orderDetailViewModal.orderDetail(Networking.wrapParams(result)).observe(this , Observer {
            it?.let {
                    resource ->   when (resource.status) {
                Status.SUCCESS -> {
                    hideProgressbar()
                    resource.data?.let { response -> OrderDetailResponse(response) }
                }
                Status.ERROR -> {
                    hideProgressbar()
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                    showProgressbar()
                }
            }
            } })
    }

    private fun OrderDetailResponse(orderDetailModal: Response<OrderDetailModal>) {
        val response = orderDetailModal.body()
        if (response?.data?.code==6){
            logOutDialog(this)
            return
        }

        if (response?.status == 1) {
            val data = response.data
           data?.items.let { it?.let { it1 -> list.addAll(it1) } }
            binding.txtOrderNo.text = "Order No. : ${data?.orderId}"
            binding.txtNo.text = data?.tableRoomNo
            if (data?.qrType=="0")
                binding.txtTableno.text ="Table Number"
            else
                binding.txtTableno.text ="Room Number"
            binding.txtTime.text = TimeStamp.getServerTime(data?.createdDate.toString())
            val dateMilliSec = TimeStamp.formatToMillis(data?.createdDate.toString(), Constant.DATE_FORMAT)
            val currentMilliSec = System.currentTimeMillis()
            val elapsedMilliSec = currentMilliSec - dateMilliSec
            val minutes: Long = elapsedMilliSec / 1000 / 60
            val seconds: Long = elapsedMilliSec / 1000 % 60

            binding.txtMin.text = "${minutes}:${seconds} min"
            adapter?.notifyDataSetChanged()
            addItems(data?.additemUrl.toString())


        }else{
            showAlert(response?.data?.msg.toString())
        }

        if (response?.status == 1 || response?.status==0)
            binding.btnAddItems.visible()
        else
            binding.btnAddItems.hide()
        refreshData(getString(R.string.no_data_found), 1)
    }

    
    // Add Items Click
    fun addItems(link:String){
        binding.btnAddItems.setOnClickListener {
            openWebPage(link)
        }
    }

    /**
     * Open a web page of a specified URL
     *
     * @param url URL to open
     */
    fun openWebPage(url: String?) {
        val webpage = Uri.parse(url)
        val intent = Intent(Intent.ACTION_VIEW, webpage)
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }


   // Show Hide Place Holder
    private fun refreshData(msg: String?, code: Int) {
        binding.include.recyclerView.setLoadedCompleted()
        adapter?.notifyDataSetChanged()
        if (list.size > 0) {
            binding.include.imgNodata.hide()
            binding.include.recyclerView.visible()
        } else {
            binding.include.imgNodata.visible()
            if (code == 0)
                binding.include.imgNodata.setImageResource(R.drawable.no_internet_bg)
            else
                binding.include.imgNodata.setImageResource(R.drawable.nodata)
            binding.include.recyclerView.hide()
        }
    }

    override fun onItemSelect(position: Int, data: OrderItems, action: String) {
       // setDelivery(data)
       setDeliveryQty(data)
    }


    // Delivery set API Calling
    fun setDelivery(data: OrderItems, qty :String) {

        showProgressbar()
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("sessionKey", session.user.data?.sessionKey)
            jsonBody.put("order_id", data.orderId)
            jsonBody.put("item_id", data.id)
            jsonBody.put("qty", qty)

            result = Networking.setParentJsonData(
                Constant.DELIVERY,
                jsonBody
            )
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        orderDetailViewModal.setDelivery(Networking.wrapParams(result)).observe(this , Observer {
            it?.let {
                    resource ->   when (resource.status) {
                Status.SUCCESS -> {
                    hideProgressbar()
                    resource.data?.let { response -> setDeliveryResponse(response) }
                }
                Status.ERROR -> {
                    hideProgressbar()
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
                Status.LOADING -> {
                    showProgressbar()
                }
            }
            } })

    }

    private fun setDeliveryResponse(deliveryModal: Response<JsonElement>) {
        val response = deliveryModal.body()

        try {
            val gson = Gson()
            val deliveryModal = gson.fromJson(response.toString(), DeliveryModal::class.java)
            if (deliveryModal?.status == 1) {
                getOrderDetail()
            }
        }catch (e :Exception){
            errorResponse(response.toString())
        }


    }




    // LogOut Dialog Show
    fun setDeliveryQty(data: OrderItems){

        val pendingQty = data.qty?.toInt()?.minus(data.deliveredQty?.toInt()!!)
        val txt ="Total Qty: ${data.qty} , Pending Qty: $pendingQty"

        val dialog = SetDeliveryDialog.newInstance(
            this@OrderDetailActivity,
            object : SetDeliveryDialog.onItemClick {
                override fun onItemCLicked(qty: Int) {
                     setDelivery(data, qty.toString())
                }
            })
        val bundle = Bundle()
        bundle.putString(Constant.TITLE, getString(R.string.app_name))
        bundle.putString(Constant.TEXT, txt)
        pendingQty?.let { bundle.putInt(Constant.QTY, it) }
        dialog.arguments = bundle
        dialog.show(supportFragmentManager, "YesNO")
    }
}
