package com.eisuchi.eisuchi.ui.order


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eisuchi.eisuchi.data.modal.OrderDataItem
import com.eisuchi.eisuchi.databinding.RowOrderListBinding
import com.eisuchi.eisuchi.uitils.Constant
import com.eisuchi.eisuchi.uitils.TimeStamp
import com.eisuchi.utils.SessionManager


class OrderListAdapter(
    private val mContext: Context,
    var list: MutableList<OrderDataItem> = mutableListOf(),
    var session: SessionManager,
    var status: String,
    private val listener: OrderListAdapter.OnItemSelected,
) : RecyclerView.Adapter<OrderListAdapter.ItemHolder>() {

    lateinit var binding: RowOrderListBinding

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        binding = RowOrderListBinding.inflate(
            LayoutInflater
                .from(parent.getContext()), parent, false
        )
        return ItemHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        val data = list[position]
        holder.bindData(mContext, data, listener, session)
    }

    interface OnItemSelected {
        fun onItemSelect(position: Int, data: OrderDataItem, action: String)
    }

    class ItemHolder(containerView: RowOrderListBinding) : RecyclerView.ViewHolder(containerView.root) {
        val binding = containerView

        fun bindData(
            context: Context,
            data: OrderDataItem,
            listener: OnItemSelected, session: SessionManager
        ) {

            binding.txtOrderNo.text = "Order No. : ${data.orderId}"
            binding.txtNo.text = data.tableRoomNo
            if (data.qrType=="0")
                binding.txtTableno.text ="Table Number"
            else
                binding.txtTableno.text ="Room Number"
            binding.txtTime.text = TimeStamp.getServerTime(data.createdDate.toString())
            val dateMilliSec = TimeStamp.formatToMillis(data.createdDate.toString(), Constant.DATE_FORMAT)
            val currentMilliSec = System.currentTimeMillis()
            val elapsedMilliSec = currentMilliSec - dateMilliSec


            val minutes: Long = elapsedMilliSec / 1000 / 60
            val seconds: Long = elapsedMilliSec / 1000 % 60

            binding.txtMin.text = "${minutes}:${seconds} min"
            binding.btnDetail.setOnClickListener { listener.onItemSelect(adapterPosition, data, "MainView") }

        }
    }


}