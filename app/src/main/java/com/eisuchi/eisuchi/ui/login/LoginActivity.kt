package com.eisuchi.eisuchi.ui.login

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.eisuchi.extention.*
import com.eisuchi.eisuchi.R
import com.eisuchi.eisuchi.data.modal.LoginModal
import com.eisuchi.eisuchi.data.network.ApiHelper
import com.eisuchi.eisuchi.data.network.Networking
import com.eisuchi.eisuchi.databinding.ActivityLoginBinding
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.eisuchi.ui.base.ViewModelProviderFactory
import com.eisuchi.eisuchi.ui.order.OrderListActivity
import com.eisuchi.eisuchi.uitils.Constant
import com.eisuchi.eisuchi.uitils.Logger
import com.eisuchi.eisuchi.uitils.Status
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response


class LoginActivity : BaseActivity() {
    lateinit var binding: ActivityLoginBinding
    private lateinit var loginViewModal: LoginViewModal

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setupViewModel()
        clickEvent()

    }


    private fun setupViewModel() {
        val factory =   ViewModelProviderFactory(ApiHelper(Networking(this).getServices()))
        loginViewModal = ViewModelProvider(this, factory).get(LoginViewModal::class.java)
    }

    // view click handle
    fun clickEvent(){
        binding.btnLogin.setOnClickListener { validation() }

        binding.edtPassword.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                validation()
            }
            false
        })

        binding.txtForgotpwd.setOnClickListener {

        }
    }

    // check input validation
    fun validation() {
        when {
            binding.edtEmail.isEmpty() -> {
                binding.mainView.showSnackBar(getString(R.string.enter_email))
            }
            !isValidEmail(binding.edtEmail.getValue())-> {
                binding.mainView.showSnackBar(getString(R.string.enter_valid_email))
            }
            binding.edtPassword.getValue().length < 6 -> {
                binding.mainView.showSnackBar(getString(R.string.enter_valid_password))
            }

            else -> {

                FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
                    if (!task.isSuccessful) {
                        Log.w("", "Fetching FCM registration token failed", task.exception)
                        return@OnCompleteListener
                    }

                    // Get new FCM registration token
                    val token = task.result
                    login(token.toString())
                    // Log and toast
                    // val msg = getString(R.string.msg_token_fmt, token)
                    Log.d("token", token.toString())
                    //  Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
                })

            }


        }
    }



    // call login API
    private fun login(token:String) {

        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("email", binding.edtEmail.getValue())
            jsonBody.put("password", binding.edtPassword.getValue())
            jsonBody.put("deviceToken", token)
            jsonBody.put("deviceType", 0)

            result = Networking.setParentJsonData(
                Constant.LOGIN,
                jsonBody
            )
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        loginViewModal.login(Networking.wrapParams(result)).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                       hideProgressbar()
                        resource.data?.let { response -> handleResponse(response) }
                    }
                    Status.ERROR -> {
                       hideProgressbar()
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                      showProgressbar()
                    }
                }
            }
        })
    }

    fun handleResponse(loginModal : Response<LoginModal>){
        val response =loginModal.body()
        if (response?.status == 1) {
            session.user = response
            Logger.d("Session Key=== ${response.data?.sessionKey}" )
            goToActivityAndClearTask<OrderListActivity>()
        } else {
            showAlert(response?.data?.msg.toString())
        }
    }





}