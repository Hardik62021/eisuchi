package com.eisuchi.dialog

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LifecycleOwner
import com.eisuchi.eisuchi.R
import com.eisuchi.eisuchi.databinding.DialogSetdeliveryBinding
import com.eisuchi.eisuchi.uitils.Constant
import com.eisuchi.eisuchi.uitils.Logger
import com.lightmicrofinance.app.utils.BlurDialogFragment
import tech.hibk.searchablespinnerlibrary.SearchableDialog
import tech.hibk.searchablespinnerlibrary.SearchableItem


class SetDeliveryDialog(context: Context) : BlurDialogFragment(), LifecycleOwner {

    private var _binding: DialogSetdeliveryBinding? = null
    var itemQty: List<SearchableItem>? = null
    private val binding get() = _binding!!
    var qtyList: ArrayList<String> = ArrayList()
    var adapterqty: ArrayAdapter<String>? = null
    var qtyCount:Int=0
    companion object {
        private lateinit var listener: onItemClick
        fun newInstance(
            context: Context,
            listeners: onItemClick
        ): SetDeliveryDialog {
            this.listener = listeners
            return SetDeliveryDialog(context)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme_Dialog_Custom)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        _binding = DialogSetdeliveryBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateData()
        qtyViewClick()
        qtySpinnerListner()
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        _binding?.btnSave?.setOnClickListener {
            listener.onItemCLicked(qtyCount)
            dismissAllowingStateLoss()
        }
        _binding?.btnCancel?.setOnClickListener {
            dismissAllowingStateLoss()
        }

    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
    }

    private fun populateData() {
        val bundle = arguments
        if (bundle != null) {
            val title = bundle.getString(Constant.TITLE)
            val text = bundle.getString(Constant.TEXT)
             qtyCount = bundle.getInt(Constant.QTY)
            _binding?.txtTitle?.text = title
            _binding?.tvText?.text = text
            setQtyAdapter()
        }
    }

    private fun setQtyAdapter(){
        val myList: MutableList<SearchableItem> = mutableListOf()

       for (i in 1 until qtyCount+1){
           qtyList.add(i.toString())
       }
        // myList.add(SearchableItem(0, getString(R.string.select_qty)))

        for (items in qtyList.indices) {

            myList.add(
                SearchableItem(
                    items.toLong() ,
                    qtyList.get(items)
                )
            )
        }
        itemQty = myList

        adapterqty = ArrayAdapter(
            requireContext(),
            R.layout.custom_spinner_item,
            qtyList
        )
        binding.spQty.setAdapter(adapterqty)
    }
    private fun qtyViewClick() {

        binding.view2.setOnClickListener {
            itemQty?.let { it1 ->
                SearchableDialog(requireContext(),
                    it1,
                    getString(R.string.select_qty), { item, _ ->
                        binding.spQty.setSelection(item.id.toInt())
                    }).show()
            }
        }

    }

    private fun qtySpinnerListner() {
        binding.spQty.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != -1) {
                    qtyCount = qtyList.get(position).toInt()
                    }
                    Logger.d("userIDq", qtyCount.toString())
                }


            }
        }


    interface onItemClick {
        fun onItemCLicked( qty:Int)
    }



    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}








