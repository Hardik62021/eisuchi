package com.eisuchi.eisuchi.ui.order


import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.eisuchi.dialog.LogoutDialog
import com.eisuchi.eisuchi.R
import com.eisuchi.eisuchi.data.modal.LoginModal
import com.eisuchi.eisuchi.data.modal.OrderDataItem
import com.eisuchi.eisuchi.data.modal.OrderListModal
import com.eisuchi.eisuchi.data.network.ApiHelper
import com.eisuchi.eisuchi.data.network.Networking
import com.eisuchi.eisuchi.databinding.ActivityOrderListBinding
import com.eisuchi.eisuchi.ui.base.BaseActivity
import com.eisuchi.eisuchi.ui.base.ViewModelProviderFactory
import com.eisuchi.eisuchi.ui.login.LoginActivity
import com.eisuchi.eisuchi.ui.orderDetail.OrderDetailActivity
import com.eisuchi.eisuchi.uitils.Constant
import com.eisuchi.eisuchi.uitils.Status
import com.eisuchi.extention.goToActivityAndClearTask
import com.eisuchi.extention.hide
import com.eisuchi.extention.visible
import com.google.gson.Gson
import com.google.gson.JsonElement
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response


class OrderListActivity : BaseActivity(), OrderListAdapter.OnItemSelected {
    lateinit var binding: ActivityOrderListBinding
    var doubleBackToExitPressedOnce = false
    private lateinit var orderViewModal: OrderViewModal
    var adapter: OrderListAdapter? = null
    var handler = Handler()
    private val list: MutableList<OrderDataItem> = mutableListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderListBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.toolbar.txtTitle.text = getString(R.string.dashboard)
        setupViewModel()
        clickEvent()
        setupRecyclerView()
        getOrderList()
        pullToRefresh()
        autoRefresh()
        //handleNotificationClick(intent)
    }


    private fun setupViewModel() {
        val factory = ViewModelProviderFactory(ApiHelper(Networking(this).getServices()))
        orderViewModal = ViewModelProvider(this, factory).get(OrderViewModal::class.java)
    }

    // SwipeLayout Refresh
    fun pullToRefresh() {
        binding.include.swipeRefreshLayout.setOnRefreshListener {
            list.clear()
            adapter?.notifyDataSetChanged()
            getOrderList()
        }
    }

    // Recyclerview Init
    fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        binding.include.recyclerView.layoutManager = layoutManager
        adapter = OrderListAdapter(this, list, session, "", this)
        binding.include.recyclerView.adapter = adapter

    }

    // View Click Handle
    fun clickEvent() {
        binding.toolbar.txtLogout.setOnClickListener {
            val dialog = LogoutDialog.newInstance(
                this,
                object : LogoutDialog.onItemClick {
                    override fun onItemCLicked() {
                        logOut()
                    }
                })
            val bundle = Bundle()
            bundle.putString(Constant.TITLE, this.getString(R.string.app_name))
            bundle.putString(Constant.TEXT, this.getString(R.string.msg_logout))
            dialog.arguments = bundle
            dialog.show(supportFragmentManager, "YesNO")
        }
    }

    // Logout API Calling
    fun logOut() {

        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("sessionKey", session.user.data?.sessionKey)

            result = Networking.setParentJsonData(
                Constant.LOGOUT,
                jsonBody
            )
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        orderViewModal.logout(Networking.wrapParams(result)).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        hideProgressbar()
                        resource.data?.let { response -> logOutResponse(response) }
                    }
                    Status.ERROR -> {
                        hideProgressbar()
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        showProgressbar()
                    }
                }
            }
        })
    }

    // LogOut API response
    private fun logOutResponse(loginModal: Response<LoginModal>) {
        val response = loginModal.body()
        if (response?.status == 1) {
            session.clearSession()
            goToActivityAndClearTask<LoginActivity>()
        } else {
            logOutDialog(this)
        }

    }


    // Auto Refresh 1 Min
    fun autoRefresh(){
        val timedTask: Runnable = object : Runnable {
            override fun run() {
               adapter?.notifyDataSetChanged()
                handler.postDelayed(this, 60000)
            }
        }

        handler.post(timedTask)
    }

    // OrderList API calling
    fun getOrderList() {
        list.clear()
        var result = ""
        try {
            val jsonBody = JSONObject()
            jsonBody.put("sessionKey", session.user.data?.sessionKey)

            result = Networking.setParentJsonData(
                Constant.ORDERS,
                jsonBody
            )
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        orderViewModal.order(Networking.wrapParams(result)).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        binding.include.swipeRefreshLayout.isRefreshing = false
                        resource.data?.let { response -> OrderResponse(response) }
                    }
                    Status.ERROR -> {
                        binding.include.swipeRefreshLayout.isRefreshing = false
                        //  logOutDialog()
                        // showAlert(it.message.toString())
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        binding.include.swipeRefreshLayout.isRefreshing = true

                    }
                }
            }
        })
    }


    // Order API Response
    private fun OrderResponse(orderModal: Response<JsonElement>) {
        val response = orderModal.body()
        try {
            val gson = Gson()
            val orderModal = gson.fromJson(response.toString(), OrderListModal::class.java)
            if (orderModal?.status == 1) {
                list.addAll(orderModal.data)
                binding.toolbar.txtTitle.text = list.get(0).restaurantName
                adapter?.notifyDataSetChanged()

            } else {
                binding.toolbar.txtTitle.text = getString(R.string.dashboard)
            }
        } catch (e: Exception) {
            errorResponse(response.toString())

        }



        refreshData(getString(R.string.no_data_found), 1)
    }


    // Show Hide Place Holder
    private fun refreshData(msg: String?, code: Int) {
        binding.include.recyclerView.setLoadedCompleted()
        adapter?.notifyDataSetChanged()
        if (list.size > 0) {
            binding.include.imgNodata.hide()
            binding.include.recyclerView.visible()
        } else {
            binding.include.imgNodata.visible()
            if (code == 0)
                binding.include.imgNodata.setImageResource(R.drawable.no_internet_bg)
            else
                binding.include.imgNodata.setImageResource(R.drawable.nodata)
            binding.include.recyclerView.hide()
        }
    }


    // Order Item Click
    override fun onItemSelect(position: Int, data: OrderDataItem, action: String) {
        val intent = Intent(this, OrderDetailActivity::class.java)
        intent.putExtra(Constant.DATA, data.id.toString())
        startActivity(intent)
        Animatoo.animateCard(this)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()
        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

    // handle Notification Click
    fun handleNotificationClick(intent: Intent?){
        val extras = intent?.extras
        if (extras != null) {
                val bundle = intent.getBundleExtra(Constant.DATA)

                val type = bundle?.getString(Constant.ORDER)
                val orderID = bundle?.getString(Constant.ORDER_ID)

                if (type.equals("4", true)
                ) {
                    Handler(Looper.getMainLooper()).postDelayed({
                        val intent = Intent(this, OrderDetailActivity::class.java)
                        intent.putExtra(Constant.DATA, orderID)
                        startActivity(intent)
                        Animatoo.animateCard(this)
                    }, 1000)

                }
            }
    }
}
