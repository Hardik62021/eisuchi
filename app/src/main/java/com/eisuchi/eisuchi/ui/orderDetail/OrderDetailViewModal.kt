package com.eisuchi.eisuchi.ui.orderDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.eisuchi.eisuchi.data.repository.MainRepository
import com.eisuchi.eisuchi.uitils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody
import retrofit2.http.Body

class OrderDetailViewModal(private val mainRepository: MainRepository) :ViewModel(){

    fun orderDetail(@Body body: RequestBody) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.oderDetail(body)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun setDelivery(@Body body: RequestBody) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.setDeliver(body)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

}