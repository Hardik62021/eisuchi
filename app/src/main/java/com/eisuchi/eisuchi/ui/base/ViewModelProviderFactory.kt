package com.eisuchi.eisuchi.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.eisuchi.eisuchi.data.network.ApiHelper
import com.eisuchi.eisuchi.data.repository.MainRepository
import com.eisuchi.eisuchi.ui.login.LoginViewModal
import com.eisuchi.eisuchi.ui.order.OrderViewModal
import com.eisuchi.eisuchi.ui.orderDetail.OrderDetailViewModal


class ViewModelProviderFactory(private val apiHelper: ApiHelper) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModal::class.java)) {
            return LoginViewModal(MainRepository(apiHelper)) as T
        }
        if (modelClass.isAssignableFrom(OrderViewModal::class.java)) {
            return OrderViewModal(MainRepository(apiHelper)) as T
        }
        if (modelClass.isAssignableFrom(OrderDetailViewModal::class.java)) {
            return OrderDetailViewModal(MainRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}