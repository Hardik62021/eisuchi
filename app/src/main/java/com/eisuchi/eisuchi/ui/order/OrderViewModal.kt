package com.eisuchi.eisuchi.ui.order

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.eisuchi.eisuchi.data.repository.MainRepository
import com.eisuchi.eisuchi.uitils.Resource
import kotlinx.coroutines.Dispatchers
import okhttp3.RequestBody
import retrofit2.http.Body

class OrderViewModal(private val mainRepository: MainRepository) :ViewModel(){

    fun order(@Body body: RequestBody) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.oder(body)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }

    fun logout(@Body body: RequestBody) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = mainRepository.login(body)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}