package com.eisuchi.eisuchi.data.repository

import com.eisuchi.eisuchi.data.network.ApiHelper
import okhttp3.RequestBody
import retrofit2.http.Body

class MainRepository(private val apiHelper: ApiHelper) {
    suspend fun  login(@Body body: RequestBody) = apiHelper.login(body)
    suspend fun  oder(@Body body: RequestBody) = apiHelper.order(body)
    suspend fun  oderDetail(@Body body: RequestBody) = apiHelper.orderDetail(body)
    suspend fun  setDeliver(@Body body: RequestBody) = apiHelper.setDeliver(body)
}