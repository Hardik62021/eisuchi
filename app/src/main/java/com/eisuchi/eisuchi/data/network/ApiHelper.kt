package com.eisuchi.eisuchi.data.network

import okhttp3.RequestBody
import retrofit2.http.Body

class ApiHelper (private val apiInterface: APIInterface) {
    suspend fun  login(@Body body: RequestBody) = apiInterface.login(body)
    suspend fun  order(@Body body: RequestBody) = apiInterface.order(body)
    suspend fun  orderDetail(@Body body: RequestBody) = apiInterface.orderDetail(body)
    suspend fun  setDeliver(@Body body: RequestBody) = apiInterface.setDelivery(body)
}