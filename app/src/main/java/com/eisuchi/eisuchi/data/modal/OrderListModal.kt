package com.eisuchi.eisuchi.data.modal

import com.google.gson.annotations.SerializedName

data class OrderListModal(

	@field:SerializedName("data")
	 val data: List<OrderDataItem> = mutableListOf(),
	@field:SerializedName("status")
	val status: Int = 0

)


data class OrderDataItem(

	@field:SerializedName("table_room_no")
	val tableRoomNo: String? = null,

	@field:SerializedName("qr_type")
	val qrType: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("pending_items")
	val pendingItems: String? = null,

	@field:SerializedName("restaurant_id")
	val restaurantId: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("updated_date")
	val updatedDate: String? = null,

	@field:SerializedName("created_date")
	val createdDate: String? = null,

	@field:SerializedName("order_id")
	val orderId: String? = null,

	@field:SerializedName("total_items")
	val totalItems: String? = null,

	@field:SerializedName("restaurant_name")
	val restaurantName: String? = null
)
