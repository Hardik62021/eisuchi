package com.eisuchi.eisuchi.data.network


import com.eisuchi.eisuchi.data.modal.*
import com.google.gson.JsonElement
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface APIInterface {

    // Login and Logout
    @POST("user")
   suspend fun login(@Body body: RequestBody): Response<LoginModal>

    // OrderList
    @POST("orders")
    suspend fun order(@Body body: RequestBody): Response<JsonElement>

    // Order Detail
    @POST("orders")
    suspend  fun orderDetail(@Body body: RequestBody):Response <OrderDetailModal>

    // Order Detail
    @POST("orders")
    suspend fun setDelivery(@Body body: RequestBody):Response<JsonElement>

}
